FROM golang:alpine AS build
WORKDIR /go/src/myapp
COPY . .
RUN apk add git
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /main .

FROM scratch
COPY --from=build /main /
ENTRYPOINT ["/main"]
